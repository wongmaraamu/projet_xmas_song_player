$(function () {

    function randomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };

    var limit_flake = 50;
    setInterval(function () {
        let dimension = randomInt(3, 9) + "px";
        var flake = "<div class='drop animate' style='left:" + randomInt(10, window.innerWidth - 20) + "px;width:" + dimension + ";height:" + dimension + "'></div>";
        $('body').append(flake);

        var list_flake = $('.drop');
        if (list_flake.length > limit_flake) list_flake[list_flake.length - 1].remove();
    }, 200);
})

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

var songs;
var song;
var currentSongIndex = 0;
var today = moment().format("YYYY-MM-DD");
// // console.log("today", today);

var Airtable = require('airtable');
Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: 'keyIPaOLq2jbzadMH'
});
var base = Airtable.base('appyrwTmRjteGnWA5');

function getAllSongs() {
    base('Table 1').select({
        // Selecting the first 3 records in Grid view:
        maxRecords: 19,
        view: "Grid view",
        filterByFormula: "OR(IS_BEFORE({Date}, '" + today + "'),IS_SAME({Date}, '" + today + "', 'days'))",

        sort: [{ field: "Date", direction: "desc" }]
    }).eachPage(function page(records, fetchNextPage) {
        // This function (`page`) will get called for each page of records.
        console.log("records", records);


        songs = records;
        var songIndex = 0;
        song = records[0];
        updatePlayerSection(0);
        records.forEach(function (record) {
            var name = record.get('Name');
            var image = record.get('Image')[0].url;
            // // console.log("name", name);
            // // console.log("image", image);
            var modele =
                '<div class="col-lg-12 text-center" onclick="playSong(' + songIndex + ')">' +
                '<div class="p-3  text-white text-left">' +
                '<div class="float-left mr-3 rounded-circle" style="background: url(' + image + ') center / cover; width: 40px; height: 40px;"></div>' +
                name +
                '</div>' +
                '</div>';

            $("#playlist .row").append(modele);
            localStorage.getItem('Nom de la musique ' + songIndex, name);
            songIndex++;

        });

        // To fetch the next page of records, call `fetchNextPage`.
        // If there are more records, `page` will get called again.
        // If there are no more records, `done` will get called.
        fetchNextPage();

    }, function done(err) {
        song = songs[0].get('Attachments')[0].url;
        if (err) {
            console.error(err);
            return;
        }
    });

}

getAllSongs();

var audio;

function updatePlayerSection(songIndex) {
    var likes = songs[songIndex].get("Like");
    $("#nom_de_la_musique").html(songs[songIndex].get('Name'));
    $("#image_album").css("background", "url(" + songs[songIndex].get('Image')[0].url + ") no-repeat center / cover");
    Like = songs[songIndex].get('Attachments')[0].url;
    $("#btnlike").prop("title", likes + " likes").attr("onclick", "updateLikes(" + songIndex + "," + likes + ")").attr("data-original-title", likes + " likes");

}

function playSong(songIndex) {
    console.log("songIndex", songIndex);
    updatePlayerSection(songIndex);
    song = songs[songIndex].get('Attachments')[0].url;
    playAudio(song);
}
function playAudio(song) {
    if (audio) audio.pause();
    audio = new Audio(song);
    audio.play();
    document.getElementById('btnplay').style.display = "none";
    document.getElementById('btnpause').style.display = "block";
    $("#image_album").addClass("rotate");
}

function pauseAudio() {
    audio.pause();
    document.getElementById('btnplay').style.display = "block";
    document.getElementById('btnpause').style.display = "none";
    $("#image_album").removeClass("rotate");

}

document.getElementById('btnplay').onclick = function () {
    console.log("marche");
    playAudio(song);

}
document.getElementById('btnpause').onclick = function () {
    pauseAudio();
}

function playPrev() {
    currentSongIndex = (currentSongIndex == 0) ? songs.length - 1 : currentSongIndex - 1;
    playSong(currentSongIndex);
}

function playNext() {
    currentSongIndex = (currentSongIndex == (songs.length - 1)) ? 0 : currentSongIndex + 1;
    playSong(currentSongIndex);
}

function displayList(){
    document.getElementById('playlistWrapper').style.display = 'block';
    document.getElementById('player').style.display='none';
}
function displayPlayer() {
    document.getElementById('playlistWrapper').style.display = 'none';
    document.getElementById('player').style.display = 'block';
}
